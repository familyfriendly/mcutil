exports.run = (args,u,mc,e) => {
    if(!e.excused.includes(u)) return mc.stdin.write(`/tellraw ${u} {"text":"you do not have access to this command..."}\n`)
    let timer;
    if(!args[1]) timer = 30;
    else timer = args[1]
        mc.stdin.write(`/tellraw @a ["",{"text":"deleting all dropped items in "},{"text":"${timer} ","color":"gold"},{"text":"seconds..."}]\n`)
        setTimeout(() => {
            mc.stdin.write(`/kill @e[type=item]\n`)
        },timer*1000)
}
exports.config = {
    name: "clear",
    info: "clears the ground!"
}