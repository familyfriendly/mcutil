const { spawn } = require('child_process');
var fs = require('fs')
  , ini = require('ini')
var config = ini.parse(fs.readFileSync('./script/settings.ini', 'utf-8'))  
var util = require("./util.js")
var lang = require("./lang.json")
var chalk = require("chalk")
var cmds = new Map()




// Check if the selected locale exist and send an error if not
if(!lang[config.main.locale]) return console.log(`lang "${config.main.locale}" does not exist\npermitted: ${Object.keys(lang).join(", ")}`);


//send the config to cmd log
console.log(`${lang[config.main.locale][1]}:\n${chalk.blue.underline("MAIN:")}\n${Object.keys(config.main).map(ob=> `${chalk.underline(ob)}:${config.main[ob]}`).join("\n")}\n${chalk.blue.underline("config:")}\n${Object.keys(config.config).map(ob=> `${chalk.underline(ob)}:${config.config[ob]}`).join("\n")}`)
console.log(chalk.green.underline(lang[config.main.locale][0]))
console.time(`startup`)

	util.updates(config)




//start child proccess
var mc = spawn("java",['-jar', '-Xms1G','-Xmx1G', '-Dfile.encoding=utf8', config.main.path,  config.config.gui ? null:"nogui"])
mc.log = (text) => {
  mc.stdin.write(`${text}\n`)
}
let userinf = {};
let done = false;

//reload config every set time
setInterval(() => {
  try {
    var config = ini.parse(fs.readFileSync('./script/settings.ini', 'utf-8')) 
  } catch(err){
  console.log(`${chalk.red("Error in loading INI file:")} ${err}`)
  }
},config.main.reloadTime)






//child proccess stdout event 
mc.stdout.on("data", stdout => {
  //buffer => string
 let data = stdout.toString();
 if(config.main.debug) console.log(data)
//if data includes "Done" we will set the done bool to true
if(data.includes("Done")) {
  util.load(cmds)
  console.timeEnd(`startup`)
  console.log(`${chalk.blue(lang[config.main.locale][2])}`)
  done=true
}


//this code is spaghetti, clean please
if(done) {
  //get the user
  let user = data.match(/(?<=\<).+(?=\>)/)
  if(!user || !user[0]) return
  user[0] = user[0].toLowerCase()
  //get chat msg
  let chat = data.match(/(?<=\<.+\>\s).+/gi)
  //get IP and username
  let ustring = data.match(/(?<=\[.+\]: ).+(?=\[.+\])|(?<=\[.+\]: .+\[\/).+(?=\])/gi)
  if(ustring != null && ustring[0] != null && ustring[1] != null) {
    if(!userinf[ustring[0]]) userinf[ustring[0]] = {infractions:0, ip:ustring[1]}
  }
  if(chat === null) return;
  let wrdarr = chat[0].toLowerCase().split(" ")
  if(chat[0].startsWith(config.config.prefix)) {
    let com = wrdarr[0].slice(config.config.prefix.length).trim().split(/ +/g);
    com = com[0]
    if(!cmds.has(com)) return mc.stdin.write(`/tellraw ${user[0]} ["",{"text":"command \\""},{"text":"${com}","color":"yellow"},{"text":"\\" does not exist"}]\n`)
    else {
      try{
        let _run = cmds.get(com)
        _run.run(wrdarr,user[0],mc,{commands:cmds,uin:userinf,excused:config.config.excused})
      } catch (err) {
        let str64 = Buffer.from(String(Math.floor(Math.random() * 2000))).toString("base64")
        fs.writeFileSync(`./errorLogs/${str64}.txt`, err.stack)
        mc.stdin.write(`/tellraw @a ["",{"text":"command \\""},{"text":"${com}","color":"gold"},{"text":"\\" ran into an error. File of the event is saved with trace ID ${str64}"}]\n`)
      }
      
    } 
  } else {
    for(let i = 0; i < wrdarr.length; i++) {
      if(config.config.blockedwords.includes(wrdarr[i]) && !config.config.excused.includes(user[0])) {
        if(!userinf[user[0]]) userinf[user[0]] = {infractions:1, ip:"unknown"}
        else userinf[user[0]].infractions++
        mc.stdin.write(`/tellraw ${user[0]} ["",{"text":"${user[0]}","color":"aqua"},{"text":", the word \\"${wrdarr[i]}\\" is blacklisted. infraction added. "},{"text":"[${userinf[user[0]].infractions}/${config.config.maxinf}]","color":"dark_red"}]\n`)
        mc.stdin.write(`/execute at ${user[0]} run summon lightningBolt\n`)
        if(userinf[user[0]].infractions >= config.config.maxinf) {
          let str = config.config.onReached;
          str = str.replace(/@u|@inf/gi,(x) => {
            
            switch(x)
            {
              case "@u":
              x = user[0]
                break;
              case "@inf":
              x =  config.config.maxinf
                break;
            }
            return x;
          })

          mc.stdin.write(`${str}\n`)
          delete userinf[user[0]]
        }
        break;
      }
    }
  }



}

})


