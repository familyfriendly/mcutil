let _db = require("wacky-db")
db = new _db.database("./script/tags.json")

exports.run = (args,u,mc,e) => {
    if(args[2])
    {
        if(!e.excused.includes(u)) return mc.stdin.write(`/tellraw ${u} {"text":"you do not have access to this command..."}\n`)
        if(args[1] == "create") {  
            db.write(args[2],args.slice(3).join(" "))
           return  mc.stdin.write(`/tellraw ${u} ["",{"text":"tag \\""},{"text":"${args[2]}","color":"gold"},{"text":"\\" created!"}]\n`)
        }  
        if(args[1] == "delete") {         
            if(!db.exists(args[2])) return mc.stdin.write(`/tellraw ${u} ["",{"text":"tag \\""},{"text":"${args[2]}","color":"gold"},{"text":"\\" does not exist"}]\n`)
            else {
                db.delete(args[2])
               return  mc.stdin.write(`/tellraw ${u} ["",{"text":"tag \\""},{"text":"${args[2]}","color":"gold"},{"text":"\\" deleted!"}]\n`)
            }
        } 
    }
    if(!args[1]) return
    if(!db.exists(args[1])) return mc.stdin.write(`/tellraw ${u} ["",{"text":"tag \\""},{"text":"${args[1]}","color":"gold"},{"text":"\\" does not exist"}]\n`)
    let ddta = db.get(args[1])
    let locked = false;
    ddta = ddta.replace(/@u|@s|@locked|@run:\w.+ /gi, (x) => {
    switch(x)
    {
        case "@u":
        x = u
        break;
        case "@s":
        x = "§"
        break;
        case "@locked":
        if(!e.excused.includes(u)) {
            locked = true
        }
        x=""
        break;
        default:
        let m = x.match(/(?<=@run:)\w.+(?= )/gi)
        try{
            x = require(`./snippets/${m}`).snippet(args,u,mc,e)
        } catch(err) {
            mc.log(`msg ${u} §6snippet failed §r ${err}`)
        }
        
        break;
    }
    return x;
    })
    if(locked) return mc.stdin.write(`/tellraw ${u} {"text":"you do not have access to this tag..."}\n`)
    mc.stdin.write(`/tellraw @a {"text":"${ddta}"}\n`)
}
exports.config = {
    name: "tag",
    info: "tag commad"
}