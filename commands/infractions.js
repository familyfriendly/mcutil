exports.run = (args,u,mc,e) => {
    if(!e.excused.includes(u)) return mc.stdin.write(`/tellraw ${u} {"text":"you do not have access to this command..."}\n`)
    if(!args[1]) return mc.stdin.write(`/tellraw ${u} {"text":"no user attatched!"}\n`);
    if(!e.uin[args[1]]) return mc.stdin.write(`/tellraw ${u} {"text":"user \\"${args[1]}\\" does not have any infractions!"}\n`);
    return mc.stdin.write(`/tellraw ${u} ["",{"text":"user "},{"text":"${u} (ip:${e.uin[args[1]].ip}) ","color":"gold"},{"text":"has "},{"text":"${e.uin[args[1]].infractions} ","color":"dark_red"},{"text":"infraction(s)!"}]\n`)
}
exports.config = {
    name: "infractions",
    info: "displays how many infractions the user has"
}