const https = require('https');
const chalk = require("chalk")
const { exec } = require('child_process');
const fs = require("fs")
const readline = require('readline');

exports.load = (_map) => {
    let _loadedinf = []
    fs.readdirSync("./commands").map(f => {
        try {
            //require file
            if(f === "snippets") return
             let fr = require(`../commands/${f}`)
            _loadedinf.push({
                name: fr.config.name,
                info: fr.config.info,
            })
            //add command to the map
            _map.set(fr.config.name,fr)
        } catch(err) {
            //lol thats not my problem friendo. deal with it yourself
            throw err
        }
    })
    return _loadedinf
}




/*
This function acs as version control using gitlab as the thingie thing that does the thing!
*/
let vercntrl = `https://gitlab.com/api/v4/projects/12756686/repository/tags`
exports.updates = (conf) => {
    try {
    //make https request to gitlab api to fetch verion number and shit
    https.get(vercntrl, (res) => {
        let data = '';
        //chunk chunk chunk
        res.on('data', (chunk) => {
         data += chunk;
         });

         //do the thing when all data is here
        res.on('end', () => {
            data = JSON.parse(data)
            if(data[1].release.description > conf.version) {
                console.log(`${chalk.green(`new update found (${data[1].release.description})`)}`)
                console.log(`- ${data[0].release.description}`)
                if(conf.main.aupdate) {
                    console.log(`auto update: ${chalk.green("true : updating")}`)
                    loadUpdate()
                } else {
                    console.log(`auto update: ${chalk.red("false : asking")}`)
                    const rl = readline.createInterface({
                        input: process.stdin,
                        output: process.stdout
                      });
                      rl.question('do you want to update? (y/n)', (answer) => {
                        if(answer.toLocaleLowerCase() === "y") loadUpdate()
                        else console.log("ok :(")
                        rl.close();
                      });

                }

            }

          });
    })
    } catch(err) {
        console.log(`${chalk.red("could not check updates:")} ${err}`)
    }

    
}

function loadUpdate() {
    exec('git reset --hard && git pull https://gitlab.com/familyfriendly/mcutil.git',(error,stdout,stderr) => {
        console.log(`
        error: ${error ? "true":"false"}
        ${error ? `- ${error}`:""}
        stdout: ${stdout ? "true":"false"}
        ${stdout ? `- ${stdout}`:""}
        stderr: ${stderr ? "true":"false"}
        ${stderr ? `- ${stderr}`:""}
        `)
    });
}

