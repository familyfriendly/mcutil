let _db = require("wacky-db")
db = new _db.database("./script/tags.json")
let sdb = new _db.database("./script/serverdata.json")
let chalk = require("chalk")














exports.warp = function(args,u,mc,e) {
    if(args[1] == "create") {
        if(!e.excused.includes(u)) return mc.stdin.write(`/tellraw ${u} {"text":"you do not have access to this command..."}\n`)
        if(!args[2]) return mc.stdin.write(`/tellraw ${u} {"text":"no warp name attatched..."}\n`)
        if(!args[3]) return mc.stdin.write(`/tellraw ${u} {"text":"no x coords attatched..."}\n`)
        if(!args[4]) return mc.stdin.write(`/tellraw ${u} {"text":"no y coords attatched..."}\n`)
        if(!args[5]) return mc.stdin.write(`/tellraw ${u} {"text":"no z coords attatched..."}\n`)
        if(!sdb.exists("warps")) sdb.write("warps",{pp:"0 0 0"})
        let data = sdb.get("warps");
        data[args[2]] = `${args[3]} ${args[4]} ${args[5]}` 
        sdb.update("warps",data)
        return mc.stdin.write(`/tellraw ${u} ["",{"text":"warp "},{"text":"${args[2]} ","color":"gold"},{"text":"created at "},{"text":"${args[3]} ${args[4]} ${args[5]}","color":"gold"}]\n`)
    
    }
    let wrploc = sdb.get("warps");
    if(!wrploc[args[1]]) return mc.stdin.write(`/tellraw ${u} {"text":"no such warp location"}\n`)
    let tpstring = `tp ${u} ${wrploc[args[1]]}\n`
    mc.stdin.write(tpstring)
    
   
}

exports.warps = function(args,u,mc,e) {
    let wrploc = sdb.get("warps");
    return mc.stdin.write(`/tellraw ${u} {"text":"${Object.keys(wrploc).join(",")}"}\n`)
}








