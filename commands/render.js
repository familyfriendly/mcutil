const chalk = require("chalk")
function calculate(number) {
    let arr = [];
    let symbol = "@"
    let max = 3;
    for(let i = 0; i < number; i++) {
        arr.push(chalk.red(symbol))
    }
    let left = max - arr.length
    for(let i = 0; i < left; i++) {
        arr.push(chalk.green(symbol))
    }
    return arr.join("")
   
}

exports.run = (args,u,mc,e) => {
    if(!e.excused.includes(u)) return
    console.clear()
    console.log(`${chalk.underline(`user ${chalk.blue(u)} requested this infraction rendering:`)}`)
    Object.keys(e.uin).forEach((o) => {
        console.log(`${o}(${e.uin[o].ip}) [${calculate(e.uin[o].infractions)}]`)
    })
}
exports.config = {
    name: "render",
    info: "renders users currently watched"
}